import logging
import time
from pathlib import Path

import click
import numpy as np
import pandas as pd
from pykalman import KalmanFilter

from routes_tools import utils

logger = logging.getLogger(__name__)

DATA_FIELDS = ['Time Stamp', 'NOx', 'Latitude', 'Longitude', 'Altitude']
REQUIRED_FIELDS = DATA_FIELDS + ['GPS-Quality']

# settings
TIME_INTERVAL = 0.25  # seconds
MIN_GPS_QUALITY = 4
KALMAN_ITERATIONS = 10


def load_data(path: Path, min_gps_quality: int) -> pd.DataFrame:
    """
    Load .csv into a pandas DataFrame.
    """
    data = pd.read_csv(str(path))

    # use only good GPS
    good_data = data[data['GPS-Quality'] >= min_gps_quality]
    return good_data[DATA_FIELDS]


def preprocess(data: pd.DataFrame, interval: float = TIME_INTERVAL) -> pd.DataFrame:
    """
    Convert to equaly spaced time series.

    The time series will have `np.nan` values, as no interpolation is done.
    """
    TIME_COLUMN = 'Time Stamp'

    # make time series equaly spaced
    trimmed_data = data[data[TIME_COLUMN] % interval == 0]
    trimmed_data.set_index('Time Stamp')

    # drop duplicates
    trimmed_data = trimmed_data.groupby(trimmed_data.index).first()

    return trimmed_data


def interpolate(data: pd.DataFrame, iterations: int) -> pd.DataFrame:
    """
    Interpolate Latitude, Longitude and Altitude using a kalman filter.
    """
    measurements = np.ma.masked_invalid(data[['Longitude', 'Latitude', 'Altitude']])

    # vars for running the kalman filter
    T = TIME_INTERVAL  # time interval
    F = np.array(
        [
            [1, 0, 0, T, 0, 0],
            [0, 1, 0, 0, T, 0],
            [0, 0, 1, 0, 0, T],
            [0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 1],
        ]
    )
    H = np.array([[1, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0]])
    R = np.diag([1e-4, 1e-4, 100]) ** 2

    initial_state_mean = np.hstack([measurements[0, :], 3 * [0.0]])
    initial_state_covariance = np.diag([1e-4, 1e-4, 50, 1e-6, 1e-6, 1e-6]) ** 2

    kf = KalmanFilter(
        transition_matrices=F,
        observation_matrices=H,
        observation_covariance=R,
        initial_state_mean=initial_state_mean,
        initial_state_covariance=initial_state_covariance,
        em_vars=['transition_covariance'],
    )

    logger.info('Start running kalman filter')
    start = time.time()
    kf = kf.em(measurements, n_iter=iterations)
    end = time.time()
    logger.info(f'Finished in {end - start}')

    state_means, state_vars = kf.smooth(measurements)

    # write new data to dataframe
    smoothed_data = data[['Time Stamp', 'NOx']].copy()
    smoothed_data['Longitude'] = state_means[:, 0]
    smoothed_data['Latitude'] = state_means[:, 1]
    smoothed_data['Altitude'] = state_means[:, 2]
    return smoothed_data


def haversine_np(lon1: float, lat1: float, lon2: float, lat2: float):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    All args must be of equal length.

    """
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = np.sin(dlat / 2.0) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2.0) ** 2

    c = 2 * np.arcsin(np.sqrt(a))
    km = 6367.8 * c
    return km


def add_distance(data: pd.DataFrame) -> None:
    data['distance'] = (
        haversine_np(
            data['Longitude'].shift(),
            data['Latitude'].shift(),
            data.ix[1:, 'Longitude'],
            data.ix[1:, 'Latitude'],
        )
        * 1000
    )  # in m


def add_speed(data: pd.DataFrame) -> None:
    data['speed'] = data['distance'] / TIME_INTERVAL  # ms / s
    data['speed'] = data['speed'] * 3.6  # km / h


def add_accelaration(data: pd.DataFrame) -> None:
    speed_diff = data['speed'].rolling(2).apply(lambda x: x[1] - x[0])
    data['acceleration'] = speed_diff / TIME_INTERVAL


def interpolate_no(data: pd.DataFrame) -> None:
    index_free = data.copy().reset_index(drop=True, inplace=False)
    data['NOx'] = index_free[['NOx']].interpolate(method='cubic').values


@click.command(
    help=(
        f"Convert NOx data from combustion to a time series. "
        f"The .csv file requires the following columgs: {', '.join(REQUIRED_FIELDS)}."
    )
)
@click.argument('csv_file', type=utils.PathPath(dir_okay=False, exists=True))
@click.argument('h5_file', type=utils.PathPath(dir_okay=False, writable=True))
@click.option(
    '-g',
    '--gps_quality',
    type=click.INT,
    default=MIN_GPS_QUALITY,
    help="Define the least required gps signal strength. Should be between 0 and 5.",
)
@click.option(
    '-i', '--iterations',
    type=click.INT,
    default=KALMAN_ITERATIONS,
    help="Number of iterations used for the kalman filter.",
)
def run(csv_file: Path, h5_file: Path, gps_quality: int, iterations: int) -> None:
    data = load_data(csv_file, gps_quality)
    data = preprocess(data)

    data = interpolate(data, iterations)

    # derive new values
    add_distance(data)
    add_speed(data)
    add_accelaration(data)
    interpolate_no(data)

    print(data.head())

    data.to_hdf(str(h5_file), key='df', mode='w')


if __name__ == '__main__':
    run()
