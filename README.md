# routes-tools

## Requirements

* Python 3.6 or newer


## Available tools

### cleanup-combustion-data

```
Usage: cleanup-combustion-data [OPTIONS] CSV_FILE H5_FILE

  Convert NOx data from combustion to a time series. The .csv file requires
  the following columgs: Time Stamp, NOx, Latitude, Longitude, Altitude,
  GPS-Quality.

Options:
  -g, --gps_quality INTEGER  Define the least required gps signal strength.
                             Should be between 0 and 5.
  -i, --iterations INTEGER     Number of iterations used for the kalman filter.
  --help                     Show this message and exit.
```


## Development

This project uses [poetry](https://poetry.eustace.io/) for packaging and
managing all dependencies and [pre-commit](https://pre-commit.com/) to run
[flake8](http://flake8.pycqa.org/) and [black](https://github.com/python/black).

Clone this repository and run

```bash
poetry develop
```

to create a virtual enviroment containing all dependencies.
Afterwards, You can run the test suite using

```bash
poetry run pytest
```

This repository follows the [Conventional Commits](https://www.conventionalcommits.org/)
style.
